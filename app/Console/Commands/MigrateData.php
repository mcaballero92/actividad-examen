<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Actividad;
use App\Models\Dependencia;
use App\Models\DetalleActividad;

class MigrateData extends Command
{
    const URL_JSON = 'http://sigo-queretaro.centralus.cloudapp.azure.com:8080/release19/C_pat/getCatalogoPOA';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:db {--local}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrar información de un JSON a un BD';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        $actividad = Actividad::find(4768);
        if ($this->option('local'))  {
            $json = file_get_contents(url('api/examen/json/getCatalogoPOA.json'));    
        } else {
            $json = file_get_contents(self::URL_JSON);
        }
        
        $jsonArray = json_decode($json, true);
        $emails = [];

        $this->output->text('Leyendo JSON de URL ' . self::URL_JSON);
        $bar = $this->output->createProgressBar(count($jsonArray['datos']));
        $bar->start();
        foreach ($jsonArray['datos'] as $item) {
            if((@$dependencia = Dependencia::query()->where('vDependencia','like','%'.$item['dependenciaEjecutora'].'%')->first())) {
                if (isset($dependencia->actividad->detalles)) {
                    $dependencia->actividad->detalles()->update(['nPresupuestoAutorizado'=>$item['aprobado']]);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        return 1;
    }
}
