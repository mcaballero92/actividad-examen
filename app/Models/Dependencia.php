<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dependencia extends Model
{
    use HasFactory;
    protected $table = 'dependencia';
    protected $primaryKey = 'iIdDependencia';
    protected $fillable = [
    'vDependencia',
    'vNombreCorto',
    'iActivo',
    'iIdComite',
    ];
    public $timestamps = false;

    public function actividad() {
        return $this->hasOne(actividad::class,'iIdDependencia', 'iIdDependencia');
    }
}
