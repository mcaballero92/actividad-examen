<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    use HasFactory;
    protected $table = 'actividad';
    protected $primaryKey = 'iIdActividad';
    protected $fillable = [
    'vActividad',
    'vObjetivo',
    'vPoblacionObjetivo',
    'vDescripcion',
    'iActivo',
    'iIdDependencia',
    'vNombreActividad',
    'iImportante',
    'vResponsable',
    'vCargo',
    'vCorreo',
    'vTelefono',
    'iODS',
    'iReto',
    'vJustificaCambio',
    'vAccion',
    'vEstrategia',
    'iideje',
    'vtipoactividad',
    'vcattipoactividad',
    ];
    public $timestamps = false;
    
    public function detalles() {
        return $this->hasMany(DetalleActividad::class,'iIdActividad','iIdActividad');
    }

    public function dependencia() {
        return $this->hasOne(Dependencia::class,'iIdDependencia', 'iIdDependencia');
    }
}
