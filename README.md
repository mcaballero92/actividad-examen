# Getting started

## Instalación

Primero clonar el proyecto en tu carpeta

    SSH
    git@gitlab.com:mcaballero92/actividad-examen.git

    HTTPS
    https://gitlab.com/mcaballero92/actividad-examen.git

Entrar a la carpeta

    cd actividad-examen

Instalar dependencias de composer (instalar composer e indicar la versión de PHP que se esta ejecutando)

    composer install

Copiar el archivo env de ejemplo y modificar la configuración hacia la base de datos en donde se va a actualizar la información

    cp .env.example .env o copy .env.example .env

Asegurarse que la url del localhost indique la ruta en donde esta el proyecto ejemplo:

    APP_URL=http://localhost/actividad-examen/public

    Si usas VirtualHost
    APP_URL=http://actividad-examen.local/

Generar llave de aplicación

    php artisan key:generate

Al final ejecutar el commando para que el script lea el JSON y actualice la base de datos

    php artisan migrate:db

Si la url esta caída o arroja 404 se puede ejecutar este comando para leer el json guardado en el proyecto

    php artisan migrate:db --local

Se adjunta archivo .env de ejemplo

    APP_NAME=Laravel
    APP_ENV=local
    APP_KEY=base64:jOlD8rax3pxD0kkCQBOoT1ldSh3bLXa/VPYa/v6fSRE=
    APP_DEBUG=true
    APP_URL=http://localhost/actividad-examen/public

    LOG_CHANNEL=stack
    LOG_DEPRECATIONS_CHANNEL=null
    LOG_LEVEL=debug

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=examen
    DB_USERNAME=root
    DB_PASSWORD=

    BROADCAST_DRIVER=log
    CACHE_DRIVER=file
    FILESYSTEM_DISK=local
    QUEUE_CONNECTION=sync
    SESSION_DRIVER=file
    SESSION_LIFETIME=120

    MEMCACHED_HOST=127.0.0.1

    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379

    MAIL_MAILER=smtp
    MAIL_HOST=mailhog
    MAIL_PORT=1025
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null
    MAIL_FROM_ADDRESS="hello@example.com"
    MAIL_FROM_NAME="${APP_NAME}"

    AWS_ACCESS_KEY_ID=
    AWS_SECRET_ACCESS_KEY=
    AWS_DEFAULT_REGION=us-east-1
    AWS_BUCKET=
    AWS_USE_PATH_STYLE_ENDPOINT=false

    PUSHER_APP_ID=
    PUSHER_APP_KEY=
    PUSHER_APP_SECRET=
    PUSHER_APP_CLUSTER=mt1

    MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
    MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
